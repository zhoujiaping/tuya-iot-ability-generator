package util

class ContextUtil {
    def static p = ~/\/src\/(test|main).*/
    /**
     * 运行脚本或者main方法,都返回项目根路径
     * */
    static String projectRoot(){
        p.matcher(System.getProperty("user.dir")).replaceFirst("")
    }
}
