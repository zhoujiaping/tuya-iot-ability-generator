import org.openqa.selenium.By
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.interactions.Actions

/**
 * 一个使用selenium操作浏览器的demo
 *
 * 具体效果是 访问某个网站,然后打开其左侧第一层所有菜单.
 * */
def home = System.getProperty("user.home")
def userDir = System.getProperty("user.dir")///home/zh/IdeaProjects/tuya-iot-ability-generator/test/groovy
System.setProperty("webdriver.chrome.driver",  "$home/dev/chromedriver")// chromedriver服务地址

def driver = new ChromeDriver()
JavascriptExecutor js = driver
driver.get("https://developer.tuya.com/cn/docs/cloud/")

println driver.title

def level1Nodes = driver.findElements(By.cssSelector("li.doc-tree-node-level-1"))
level1Nodes.each{
    def a = it.findElement(By.cssSelector("span.ant-tree-node-content-wrapper a"))
    def name = a.text.replaceAll(/([^\u4e00-\u9fa5\w_-])+/, "")
    println("name=$name,url=${a?.getAttribute('href')}")
    def switcher = it.findElements(By.cssSelector("span.ant-tree-switcher"))
    if(switcher){
        js.executeScript("""
   arguments[0].click();
""", switcher[0])
    }
    def level2Nodes = it.findElements(By.cssSelector("li.doc-tree-node-level-2"))
    level2Nodes.each{
        level2Node->
            def a2 = level2Node.findElement(By.cssSelector("span.ant-tree-node-content-wrapper a"))
            waitUntil(10, 50) { a2.text }
            def name2 = a2.text.replaceAll(/([^\u4e00-\u9fa5\w_-])+/, "")
            println("name=$name2,url=${a2?.getAttribute('href')}")
    }
}
//
def links = driver.findElements(By.cssSelector("span.ant-tree-node-content-wrapper a"))
def pattern = ~'^https://developer.tuya.com/cn/docs/cloud/[a-zA-Z0-9_-]{10}\\?id=[a-zA-Z0-9_-]{13}$'
links.findAll{
    it.getAttribute("href").matches(pattern) && !it.text.matches(/\s*API\s*列表\s*/)
}.each{
    println it
}

def waitUntil(int timeoutSeconds, int sleepMillis, Closure closure) {
    long leftMillis = timeoutSeconds * 60 * 1000
    while (!closure()) {
        if (leftMillis < 0) {
            throw new RuntimeException("timeout")
        }
        Thread.sleep(sleepMillis)
        leftMillis -= sleepMillis
    }
}




