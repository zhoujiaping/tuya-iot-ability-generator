import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.chrome.ChromeDriver

/**
 * 效果: 自动打开谷歌浏览器访问某个页面,下载这个页面的内容.
 * */
def home = System.getProperty("user.home")
def userDir = System.getProperty("user.dir")///home/zh/IdeaProjects/tuya-iot-ability-generator/test/groovy
System.setProperty("webdriver.chrome.driver", "$home/dev/chromedriver")// chromedriver服务地址

WebDriver driver = new ChromeDriver()
driver.get("https://developer.tuya.com/cn/docs/cloud/80bb968f1d?id=Ka7kjv3j8jgvr")

String title = driver.getTitle()
System.out.printf(title)

def elements = driver.findElements(By.cssSelector("[class^=docDetail_breadcrumb] span"))
def pathParts = elements.collect{
    it.text.replaceAll(/\s+/,"")
}

def dirName = pathParts[0..-2].join('/')
def fileName = pathParts[-1]
def dir = new File("$userDir/../resources/text/$dirName")
dir.mkdirs()
/*
ele = driver.findElement(By.cssSelector("[class='comp-markdown-time']"))
println "time = ${ele.text}"

ele = driver.findElement(By.cssSelector("[class='comp-markdown-title']"))
println "methodDesc = ${ele.text}"*/

WebElement body = driver.findElement(By.tagName("body"))
println "text = ${body.text}"

new File(dir,fileName).setText(body.text,"utf-8")




//docDetail_breadcrumb__2g45w

driver.close()